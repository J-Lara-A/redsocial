using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class operaciones
    {
        public double Resta(double num1, double num2)
        {
            double Val = num1 - num2;
            return Val;
        }

        public double Suma(double num1, double num2)
        {
            double Val = num1 + num2;
            return Val;
        }

        public double Multi(double num1, double num2)
        {
            double Val = num1 * num2;
            return Val;
        }

        public double Div(double num1, double num2)
        {
            double Val = num1 / num2;
            return Val;
        }

    }
}
