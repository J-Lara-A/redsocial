using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //instanciacion o como se llame
            operaciones objeto_operaciones = new operaciones();

            double a, b, resultado;
            a = Convert.ToDouble(txt1.Text);
            b = Convert.ToDouble(txt2.Text);
            resultado = objeto_operaciones.Resta(a, b);


            MessageBox.Show("El resultado es: " + resultado);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            operaciones objeto_operaciones = new operaciones();

            double a, b, resultado;
            a = Convert.ToDouble(txt1.Text);
            b = Convert.ToDouble(txt2.Text);
            resultado = objeto_operaciones.Suma(a, b);


            MessageBox.Show("El resultado es: " + resultado);
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            operaciones objeto_operaciones = new operaciones();

            double a, b, resultado;
            a = Convert.ToDouble(txt1.Text);
            b = Convert.ToDouble(txt2.Text);
            resultado = objeto_operaciones.Multi(a, b);


            MessageBox.Show("El resultado es: " + resultado);
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            operaciones objeto_operaciones = new operaciones();

            double a, b, resultado;
            a = Convert.ToDouble(txt1.Text);
            b = Convert.ToDouble(txt2.Text);
            resultado = objeto_operaciones.Resta(a, b);


            MessageBox.Show("El resultado es: " + resultado);
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            operaciones objeto_operaciones = new operaciones();

            double a, b, resultado;
            a = Convert.ToDouble(txt1.Text);
            b = Convert.ToDouble(txt2.Text);
            resultado = objeto_operaciones.Div(a, b);


            MessageBox.Show("El resultado es: " + resultado);
        }

        private void clear_Click(object sender, EventArgs e)
        {
            txt1.Text = "";
            txt2.Text = "";
           
        }
    }
}
